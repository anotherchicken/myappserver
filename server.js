var express = require('express')
,app = express()
,cors = require('cors')
,fs = require("fs");

var originsWhitelist = [
    'http://devchickenstudio.ca:4200',      //this is my front-end url for development
];

var corsOptions = {
        origin: function(origin, callback){
              var isWhitelisted = originsWhitelist.indexOf(origin) !== -1;
              callback(null, isWhitelisted);
        },
        credentials:true
      }

app.use(cors(corsOptions));

app.get('/api/listHeroes', function (req, res) {
   fs.readFile( __dirname + "/" + "heros.json", 'utf8', function (err, data) {
      console.log( data );
      res.end( data );
   });
})

app.get('/api/hero/:id', function (req, res) {
    // First read existing users.
    fs.readFile( __dirname + "/" + "heros.json", 'utf8', function (err, data) {
       var users = JSON.parse( data );
       var user = users.filter(function(o){return (o.id == req.params.id); });
       console.log( user );
       res.end( JSON.stringify(user));
    });
 })

 function filterItems(query) {
   return fruits.filter(function(el) {
       return el.toLowerCase().indexOf(query.toLowerCase()) > -1;
   })
 }
 
 app.get('/api/hero/name/:name', function (req, res) {
   // First read existing users.
   fs.readFile( __dirname + "/" + "heros.json", 'utf8', function (err, data) {
      var users = JSON.parse( data );
      var user = users.filterItems(req.params.name);
      console.log( user );
      res.end( JSON.stringify(user));
   });
})

 
var server = app.listen(8081, function () {
   var host = server.address().address
   var port = server.address().port
   console.log("Example app listening at http://%s:%s", host, port)
})